/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#ifndef _GESTIONUSUARIOS_H_RPCGEN
#define _GESTIONUSUARIOS_H_RPCGEN

#include <rpc/rpc.h>


#ifdef __cplusplus
extern "C" {
#endif


struct struc_login {
	char usuario[20];
	char password[20];
};
typedef struct struc_login struc_login;

struct struc_sesion {
	char usuario[20];
	char tipo[12];
};
typedef struct struc_sesion struc_sesion;

struct struc_nuevo_usuario {
	char nombre_apellido[30];
	char identificacion[12];
	char usuario_unicauca[10];
	char password[10];
	char tipo[12];
};
typedef struct struc_nuevo_usuario struc_nuevo_usuario;

#define PROG_GESTION_USUARIOS 0x20000001
#define VERS_GESTION_USUARIOS 1

#if defined(__STDC__) || defined(__cplusplus)
#define iniciar_sesion 1
extern  struc_sesion * iniciar_sesion_1(struc_login *, CLIENT *);
extern  struc_sesion * iniciar_sesion_1_svc(struc_login *, struct svc_req *);
#define reg_nuevo_usuario 2
extern  bool_t * reg_nuevo_usuario_1(struc_nuevo_usuario *, CLIENT *);
extern  bool_t * reg_nuevo_usuario_1_svc(struc_nuevo_usuario *, struct svc_req *);
extern int prog_gestion_usuarios_1_freeresult (SVCXPRT *, xdrproc_t, caddr_t);

#else /* K&R C */
#define iniciar_sesion 1
extern  struc_sesion * iniciar_sesion_1();
extern  struc_sesion * iniciar_sesion_1_svc();
#define reg_nuevo_usuario 2
extern  bool_t * reg_nuevo_usuario_1();
extern  bool_t * reg_nuevo_usuario_1_svc();
extern int prog_gestion_usuarios_1_freeresult ();
#endif /* K&R C */

/* the xdr functions */

#if defined(__STDC__) || defined(__cplusplus)
extern  bool_t xdr_struc_login (XDR *, struc_login*);
extern  bool_t xdr_struc_sesion (XDR *, struc_sesion*);
extern  bool_t xdr_struc_nuevo_usuario (XDR *, struc_nuevo_usuario*);

#else /* K&R C */
extern bool_t xdr_struc_login ();
extern bool_t xdr_struc_sesion ();
extern bool_t xdr_struc_nuevo_usuario ();

#endif /* K&R C */

#ifdef __cplusplus
}
#endif

#endif /* !_GESTIONUSUARIOS_H_RPCGEN */
