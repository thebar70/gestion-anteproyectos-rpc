struct struc_usuario{
    char nombre[30];
    int identificacion[10];
    char usuario_unicauca[10];
    char password[10];
    char rol_jef[20];
};
struct struc_datos_anteproyecto{
    char modalidad[12];
    char titulo_anteproyecto[40];
    char nom_estudiante1[30];
    char nom_estudiante2[30];
    char nom_director[30];
    char nom_codirector[30];
    char fecha_reg_anteproyecto[10];
    char fecha_aprovacion[10];
    char estado[10];
    char concepto[10];
    char numero_revision[12];
    char codigo_anteproyecto[3];
};
struct struc_evaluadores{
    char codigo_anteproyecto[10];
    char nombre_evaluador1[30];
    char nombre_evaluador2[30];
    char concepto_evaluador1[30];
    char concepto_evaluador2[30];
    char fecha_revision1[10];
    char fecha_revision2[10];
};
struct struc_info_basica_anteproyecto{
    char codigo_anteproyecto[10];
    char titulo_anteproyecto[40];
};

/*typedef struct struc_informacion_general *proxNodoInformacionAnteproyecto;*/
struct struc_informacion_general{
    char titulo_anteproyecto[40];
    char modalidad[12];
    char nom_estudiante1[30];
    char nom_estudiante2[30];
    char nom_director[30];
    char nom_codirector[30];
    char fecha_reg_anteproyecto[10];
    char estado_anteproyecto[10];
    char concepto_anteproyecto[10];

    char concepto[10];
    char codigo_anteproyecto[10];
    char nombre_evaluador1[30];
    char nombre_evaluador2[30];
    char concepto_evaluador1[30];
    char concepto_evaluador2[30];
    /*proxNodoInformacionAnteproyecto nodoSiguiente;*/
};

typedef struct struc_total_evaluadores *proxNodoEvaluador;
struct struc_total_evaluadores{
    struct struc_evaluadores evaluador;
    proxNodoEvaluador nodoSiguiente;
};

typedef struct struc_total_anteproyectos *proxNodoAnteproyecto;
struct struc_total_anteproyectos{
    struct struc_datos_anteproyecto anteproyecto;
    proxNodoAnteproyecto nodoSiguiente;
};

program PROG_GESTION_ANTEPROYECTOS{
    version VERS_GESTION_ANTEPROYECTOS{
        
        bool registrar_usuario(struc_usuario usuario) = 1;
        bool registrar_anteproyecto(struc_datos_anteproyecto datos_anteproyecto) = 2;
        bool asignar_evaluador(struc_evaluadores) = 3;
        struc_informacion_general buscar_anteproyecto(string codigo_anteproyecto) = 4;
        proxNodoAnteproyecto todos_anteproyectos(void) = 5;
        bool cambiar_concepto_anteproyecto_jefe(string codigo_anteproyecto) = 6;
        bool cambiar_concepto_anteproyecto_evaluador(string codigo_anteproyecto) = 7;
        struc_info_basica_anteproyecto buscar_info_basica_anteproyecto(string codigo_anteproyecto)=8;

        /*Funcionalidades estudiante*/
        struc_info_basica_anteproyecto consultar_anteproyecto_estudiante(string codigo_anteproyecto)=9;
        /*listar anteproyectos Todos??*/


    } = 1;
}= 0x20000003;
 
