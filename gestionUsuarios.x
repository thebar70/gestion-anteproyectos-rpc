struct struc_login{
    char usuario[20];
    char password[20];
	
};
struct struc_sesion{
    char usuario[20];
    int tipo;
};
struct struc_nuevo_usuario{
    char nombre_apellido[30];
    char identificacion[12];
    char usuario_unicauca[10];
    char password[10];
    char tipo[12];
};
program PROG_GESTION_USUARIOS{
    version VERS_GESTION_USUARIOS{
        struc_sesion iniciar_sesion(struc_login)= 1;
        bool reg_nuevo_usuario(struc_nuevo_usuario) = 2;
    } = 1;
}= 0x20000001;
